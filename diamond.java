public class diamond {
    public static void main(String[] args) {
        int n=5;
        for(int i=1; i<=n; i+=2){
            for(int j=0; j<(n-i)/2; j++){
                System.out.print(" ");
            }
            for(int k=1; k<=i; k++){
                System.out.print("*");
            }
            System.out.println();
        }
        for(int i=n-2; i>=1; i-=2){
            for(int j=(n-i)/2; j>0; j--){
                System.out.print(" ");
            }
            for(int k=i; k>=1; k--){
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
