import java.util.Scanner;
public class segitigapascal {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Pascal Triangle \n");
		
		System.out.print("Masukkan tinggi baris : ");
		int high = input.nextInt();
		
		for (int i=0; i<high; i++){
			for (int j=1; j<=(high-i)*2; j++){
				System.out.print(" ");
			}
			int number = 1;
			for (int k=0; k<=i; k++){
				System.out.print(number+"   ");
				number = number * (i-k)/(k+1);
			}
			System.out.println();
		}
	}
}
